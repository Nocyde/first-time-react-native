/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 */

import React from 'react';
// import type {Node} from 'react';
import {
  Button,
  Vibration,
  Share,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from 'react-native';

const Section = ({children, title})=> {
  return (
    <View style={styles.sectionContainer}>
      <Text
        style={[
          styles.sectionTitle,
        ]}>
        {title}
      </Text>
      <Text
        style={[
          styles.sectionDescription,
        ]}>
        {children}
      </Text>
    </View>
  );
};

const App = () => {
  return (
    <SafeAreaView>
      <StatusBar barStyle='light-content' />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        >
        <View>
          <Section title="Application de l'anniversaire de Quentin">
            Appuie sur le bouton pour célébrer cet anniversaire
          </Section>
          <Section>
          <Button title="Bouton" onPress={() => Vibration.vibrate()} />
          </Section>
          <Section>
          <Button
            onPress={() => {
              Share.share({message: "C'est mon anniversaire !"});
            }}
            title="Partager"
            color="#841584"
            accessibilityLabel="Learn more about this purple button"
          />
          </Section>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
